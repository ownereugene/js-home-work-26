const slider = document.querySelector(".slider-line");
const sliderItems = Array.from(slider.children);
const buttonRight = document.querySelector(".arrow-right");
const buttonLeft = document.querySelector(".arrow-left");

sliderItems.forEach((slide, index) => {
  if (index !== 0) slide.classList.add("hidden");

  slide.dataset.index = index;
  sliderItems[0].setAttribute("data-active", "");

  slide.addEventListener("click", () => {
    showNextSlide("next");
  });
});

buttonRight.addEventListener("click", () => {
  showNextSlide("next");
});
buttonLeft.addEventListener("click", () => {
  showNextSlide("prev");
});

function showNextSlide(direction) {
  const currentSlide = slider.querySelector("[data-active]");
  const currenSlideIndex = +currentSlide.dataset.index;
  currentSlide.classList.add("hidden");
  currentSlide.removeAttribute("data-active");

  let nextSlideIndex;

  if (direction === "next") {
    nextSlideIndex =
      currenSlideIndex + 1 === sliderItems.length ? 0 : currenSlideIndex + 1;
  } else if (direction === "prev") {
    nextSlideIndex =
      currenSlideIndex === 0 ? sliderItems.length - 1 : currenSlideIndex - 1;
  }

  const nextSlide = slider.querySelector(`[data-index='${nextSlideIndex}']`);
  nextSlide.classList.remove("hidden");
  nextSlide.setAttribute("data-active", "");
}
